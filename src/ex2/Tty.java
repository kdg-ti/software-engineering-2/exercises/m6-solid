package ex2;

import static ex2.OutputDevice.PRINTER;

public class Tty {
  public void copy(Keyboard kbd, OutputDevice dev) {
    char c;
    while (kbd.hasMore()) {
      c=kbd.readKeyboard();
      if (dev == PRINTER) {
        writePrinter(c);
      } else {
        writeDisk(c);
      }
    }
  }

  private void writePrinter(char c){
    System.out.println(c);
  }

  private void writeDisk(char c){
    System.out.println(c);
  }
}
