package ex5.filter;

import ex5.model.Product;
import ex5.model.ProductColor;
import ex5.model.ProductSize;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jan de Rijke.
 */
public class ProductFilter {
	public List<Product> ByColor(List<Product> products, ProductColor productColor) {
		List<Product> filteredProducts = new ArrayList<>();
		for (Product product : products) {
			if (product.getColor() == productColor) {filteredProducts.add(product);}
		}
		return filteredProducts;
	}

	public List<Product> BySize(List<Product> products, ProductSize productSize) {
		List<Product> filteredProducts = new ArrayList<>();
		for (Product product : products) {
			if (product.getSize() == productSize) {filteredProducts.add(product);}
		}
		return filteredProducts;
	}

	public List<Product> ByColorAndSize(
		List<Product> products,
		ProductColor productColor,
		ProductSize productSize) {
		List<Product> filteredProducts = new ArrayList<>();
		for (Product product : products) {
			if (product.getSize() == productSize && product.getColor() == productColor) {
				filteredProducts.add(product);
			}
		}
		return filteredProducts;
	}


}